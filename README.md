# README #

**¿Para qué sirve el HashMap adj?**

R/: Es ventajoso usar un HashMap para los adyacentes ya que como no se va a usar índices para identificar los vértices sino un String entonces se puede poner el nombre del vértice como llave y el contenido como el valor. Además se puede asegurar como en un arreglo de vértices, una complejidad casi constante ya que solo es necesario saber la llave. Así mismo se puede asemejar a la implementación de un grafo de símbolos (Symbol Graph) donde se pueden hallar el grado de separación entre vértices.

