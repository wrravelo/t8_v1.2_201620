package estructuras;
import java.util.*;


public class Bag <Item> implements Iterable<Item> {
	
	private Node<Item> first;
	private int N;
	
	public void add(Item item) {
		Node<Item> oldFirst = first;
		first = new Node<Item>(item);
		first.setNext(oldFirst);
		N++;
	}
	
	public boolean isEmpty() {
		return first == null;
	}
	
	
	// Iterator
	
	public Iterator<Item> iterator() {
		return new ListIterator();
	}
	
	// Private Classes
	
	private class ListIterator implements Iterator<Item> {
		
		Node<Item> current = first;
		
		public boolean hasNext() {
			return current != null;
		}
		
		public Item next() {
			Item item = current.getItem();
			current = current.getNext();
			
			return item;
		}
		
		public void remove() {}
	}
	
	private class Node <Item> {
		Item item;
		Node<Item> next;
		
		public Node(Item item) {
			this.item = item;
		}
		
		public Node<Item> getNext() {
			return next;
		}
		
		public Item getItem() {
			return item;
		}
		
		public void setNext(Node<Item> next) {
			this.next = next;
		}
	}
	
	public int size() {
		return N;
	}
}
