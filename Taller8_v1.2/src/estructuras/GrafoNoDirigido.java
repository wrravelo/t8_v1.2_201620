package estructuras;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> {

	private final int V;
	private int E;

	/**
	 * Lista de adyacencia 
	 */
	private Bag<Arco<String>>[] adj;

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido(int V) {
		//TODO implementar
		this.V = V;
		this.E = 0;
		adj = (Bag<Arco<String>>[]) new Bag[V];
		for (int v = 0; v < V; v++)
			adj[v] = new Bag<Arco<String>>();
	}


	public Iterable<Arco<String>> adj(int v) { 
		return adj[v]; 
	}

	
	public Iterable<Arco<String>> edges() {
		Bag<Arco<String>> b = new Bag<Arco<String>>();
		for (int v = 0; v < V; v++)
			for (Arco<String> e : adj[v])
				if (e.other(v) > v) b.add(e);
		return b;
	}


	public Arco<String> crearArco(Arco<String> e)
	{
		int v = e.either(), w = e.other(v);
		adj[v].add(e);
		adj[w].add(e);
		E++;
		
		return e;
	}
	
	public Arco<String>[] darArcos() {
		int s = 0;
		
		for(Bag<Arco<String>> x: adj) {
			s += x.size();
		}
		
		
		Arco<String>[] array = (Arco<String>[]) new Arco[s];
		
		int i = 0;
		for(Bag<Arco<String>> x: adj) {
			for(Arco<String> actual : x) {
				array[i] = actual;
				i++;
			}
		}
		
		return array;
	}
	
	public int V() {  return V;  }
	
	public int E() {  return E;  }
}
