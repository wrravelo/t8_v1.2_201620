package estructuras;

/**
 * Representa un Arco con peso en un grafo.
 * Cada arco consta de un Nodo inicio, un Nodo fin y un costo. Adicionalmente, el Arco puede
 * guardar informaci髇 adicional en un objeto (tipo E).
 * @param K tipo del identificador de los vertices (comparable)
 * @param E tipo de la informacion asociada a los arcos
 */
public class Arco<E> implements Comparable<Arco<E>> {

	/**
	 * Costo de ir de nodo inicio a nodo fin
	 */
	private double costo;

	/**
	 * Informacion adicional que se puede guardar en el arco
	 */
	//actualizacion
	private E obj;

	/**
	 * Nodo inicio 
	 */
	private int inicio;

	/**
	 * Nodo fin
	 */
	private int fin;


	/**
	 * Construye un nuevo arco desde un nodo inicio hasta un nodo fin
	 * con un peso dado e informaci贸n adicional. 
	 * @param inicio el nodo inicial del arco
	 * @param fin el nodo final del arco
	 * @param costo Costo del arco
	 * @param obj Informaci贸n adicional que se desea guardar
	 */
	public Arco(int inicio, int fin, double costo, E obj) {
		this.costo = costo;
		this.obj = obj;
		this.inicio = inicio;
		this.fin = fin;
	}


	/**
	 * Devuelve el nodo inicio del arco
	 * @return Nodo inicio
	 */
	public int either() {
		return inicio;
	}

	/**
	 * Devuelve el nodo final del arco
	 * @return Nodo fin
	 */
	public int other() 
	{
		return fin;
	}

	/**
	 * Devuelve el costo del arco
	 * @return costo
	 */
	public double darCosto() {
		return costo;
	}
	
	public int other(int vertex) {
		if      (vertex == inicio) return fin;
		else if (vertex == fin) return inicio;
		else return -1;
	}

	/**
	 * Asigna un objeto como informacion adicional asociada al arco
	 * @param info Objeto (tipo E) que se desea guardar como informaci贸n adicional
	 * @return este arco con la informaci贸n adicional asignada
	 */
	public  Arco<E> asignarInformacion(E info) {
		obj = info; 
		return this;
	}

	/**
	 * Devuelve la informaci贸n adicional asociada al arco
	 * @return objeto (tipo E) asociado como informaci贸n adicional
	 */
	public 	E darInformacion() {
		return obj;
	}
	
	public int compareTo(Arco<E> that) {
		 if      (this.darCosto() < that.darCosto()) return -1;
	        else if (this.darCosto() > that.darCosto()) return +1;
	        else                                    return  0;
	}
}
