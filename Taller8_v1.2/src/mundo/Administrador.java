package mundo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;
import estructuras.SymbolWeightedGraph;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";


	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	SymbolWeightedGraph grafo;

	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		grafo = new SymbolWeightedGraph();
	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{		
		return grafo.darRutasQuePasan(identificador);
	}
	
	public SymbolWeightedGraph darGrafo() {
		return grafo;
	}
	
	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		return grafo.menorDistancia().darCosto();
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		return grafo.mayorDistancia().darCosto();
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		grafo.cargarNodos(RUTA_PARADEROS);

		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");

		grafo.cargarArcos(RUTA_RUTAS);

		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion<String> buscarEstacion(String identificador)
	{
		return grafo.buscarEstacion(identificador);
	}

}
