package modelos;

import estructuras.Nodo;

/**
 * Clase que representa una estacion del metro de New York
 * @param K tipo del identificador unico de los vertices del grafo 
 */
public class Estacion<K extends Comparable<K>> implements Nodo<K>{

	/**
	 * Latitud del estación
	 */
	private double latitud;
	
	/**
	 * Longitud del estación
	 */
	private double longitud;
	
	/**
	 * Identificador único del paradero (de tipo K)
	 */
	private K identificador;
	
	
	/**
	 * Construye un nuevo paradero con un identificador y una ubicacion latitud y la longitud dados
	 * @param id Indentificador unico
	 * @param latitud 
	 * @param longitud 
	 */
	public Estacion(K id, double latitud, double longitud) {
		this.identificador = id;
		this.latitud = latitud;
		this.longitud =  longitud;
	}
	
	public K darId(){
		return identificador;
		
	}
	
	/**
	 * Devuelve la latitud del paradero
	 * @return latitud
	 */
	public double darLatitud() {
		return latitud;
	}
	
	/**
	 * Devuelve la longitud del paradero
	 * @return longitud
	 */
	public double darLongitud() {
		return longitud;
	}
	
	/**
	 * nombre (latitud, longitud)
	 */
	@Override
	public String toString() {
		return darId() +" ("+darLatitud()+" , "+darLongitud()+")";
	}

	@Override
	public int compareTo(Nodo<K> o) 
	{
		return this.darId().compareTo(o.darId());
	}
	
	
}
